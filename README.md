# Common information
## Installation Directory
### Locate Install Directory
#### PowerShell
1. Press `Win + R` on keyboard
1. Type `powershell.exe` and press enter.
1. Copy the following code and paste it in the window and press enter.
1. You should see your folder open (it may be flashing in the taskbar. Check there :D)

##### Baldur's Gate 1
```
saps (gp HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* | ? Displayname -Match "Baldur's Gate: Enhanced Edition" | select -Expand InstallLocation)
```
##### Baldur's Gate 2
```
saps (gp HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* | ? Displayname -Match "Baldur's Gate: Enhanced Edition" | select -Expand InstallLocation)
```
#### Steam
1. Open Steam
1. Right Click on the appropriate game
1. Select **Manage**
1. Select **Browse local files**
1. You should see your folder open (it may be flashing in the taskbar. Check there :D)

## WeiDU
### Copying weidu.exe to setup-_mod_.exe
This setup file is usually provided by mods but for Linux it is not. To keep things compatible with both variants, I have opted to externalize WeiDU from the download file.  This means for this specific mod (and any others that do not have the setup-_mod_.exe packaged with it), you will need to copy the weidu.exe that you will download in the first step below to the [Installation Directory](#installation-directory).  Once there, you can rename it to setup-_mod_.exe and then double click to run the executable.

> [!IMPORTANT]
> The _mod_ in setup-_mod_.exe is meant to be replaced with the mod name as listed below.
>
> For example, for **a8_nio** you would rename the file to **setup-a8_nio.exe**

> [!TIP]
> If you do not want to constantly copy/rename weidu.exe to a setup-*mod*.exe format, you can instead opt to solely copy weidu.exe to your [Installation Directory](#installation-directory) and then in a terminal (PowerShell/CMD) navigate to your [Installation Directory](#installation-directory) (using cd) and then run:
>
> `weidu.exe *mod\mod.tp2*`.
>> Example: `weidu.exe a8_nio\a8_nio.tp2`
>
> The tp2 file will always be the same name as the parent folder.

### Language
This guide assumes that you will follow the prompts to set your language when you first run weidu.  You may be asked to confirm for certain mods but it should be a one-time operation.
> [!NOTE]
> Typically 0 is English.


## Installing New Mods
If you already have mods installed, when you want to install new mods it is recommended that you simply uninstall each mod that you installed previously first, and then to follow the [Installation Order](#installation-order) listed below.

> [!NOTE]
> The only exception to this rule is with the Dlcmerger mod when used with BGEE. Dlcmerger is a staple prerequisite that must be present. It isn't harmful to remove/re-install, but it just simply isn't necessary

> [!TIP]
> You do not need to delete and re-download the previously installed mods as they are already in your game folder, and should be on the correct version.

# Instructions

## Update
The general idea is for you to remove all traces of the mod, uninstalling all components and then removing the mod folder, which you replace by downloading the new version of the mod and moving its contents back to your [Installation Directory](#installation-directory).

In practice, you simply have to follow both the Uninstallation and the Installation instructions outlined below.
1. Follow the [Uninstallation](#uninstallation) instructions below, including the removal of the a8_nio folder
1. Follow the [Install a8_nio](#install-a8_nio) instructions below, including the download of the new a8_nio mod version.

## Uninstallation
> [!IMPORTANT]
> It is imperitive that you perform this when the mod has been updated to ensure you remove all traces of the legacy codebase from your system.

> [!NOTE]
> The Final step (delete the folder) is only necessary when you are updating the a8_nio mod.  If you are uninstalling only to install new mods, you can skip this step
1. Open Your [Installation Directory](#installation-directory)
1. Run **setup-a8_nio** from your [Installation Directory](#installation-directory)
1. Select \[**U**] to uninstall Base Resource Pack
1. Select \[**U**] to uninstall the BG(2)EE Component.
1. Delete the **a8_nio** folder from your [Installation Directory](#installation-directory)

## Installation Order
1. Install Pre-Requisites
    1. WeiDU
    1. DlcMerger
    1. BGEE AI Denoised Areas
    1. DragonspearUI++
    1. Hidden Gameplay Options
1. Install a8_nio

## Install Pre-Requisites
Install the following before installing a8_nio.

### WeiDU
1. Download the latest **amd64/64-bit** [WeiDU](https://github.com/WeiDUorg/weidu/releases/latest)
1. Extract the zip file contents
1. Copy the weidu executable to your [Installation Directory](#installation-directory)
> [!NOTE]
> You only need to copy one weidu executable to your folder. The pertinent executable is:
>
> 
> |Windows  |Linux    |
> |---------|---------|
> |weidu.exe|weidu    |

### DlcMerger
> [!WARNING]
> Dlc Merger is not compatible with BG2EE and is not necessary to be downloaded for bg2ee.

1. Download [DlcMerger](https://github.com/Argent77/A7-DlcMerger/releases/latest)
1. Extract the zip file contents contents
1. Copy contents to your [Installation Directory](#installation-directory)
1. Run **setup-DlcMerger.exe** from your [Installation Directory](#installation-directory).
    1. Select \[**1**] to Merge everything.

### BG(2)EE AI Denoised Areas
Select only one of the two versions, appropriate for the current game (BGEE/BG2EE)

#### BGEE
1. Download [BGEE AI Denoised Areas](https://github.com/WillScarlettOhara/BGEE_AI_Denoised_Areas/releases/latest)
1. Extract the zip file contents
1. Copy contents to your [Installation Directory](#installation-directory)
1. Run **setup-BGEE_AI_Denoised_Areas.exe** from your [Installation Directory](#installation-directory).
    1. Select \[**I**] to install the mod.
#### BG2EE
1. Download [BGEE AI Denoised Areas](https://github.com/WillScarlettOhara/BG2EE_AI_Denoised_Areas/releases/latest)
1. Extract the zip file contents
1. Copy contents to your [Installation Directory](#installation-directory)
1. Run **setup-BGEE_AI_Denoised_Areas.exe** from your [Installation Directory](#installation-directory).
    1. Select \[**I**] to install the mod.

### Dragonspear UI++
1. Download [Draonspear UI++](https://github.com/anongit/DragonspearUI/archive/refs/heads/master.zip)
1. Extract the zip file contents
1. Copy contents to your [Installation Directory](#installation-directory)
1. Run **setup-dragonspear_ui++.exe** from your [Installation Directory](#installation-directory).
    1. Select your language
        1. \[**0**] for English
    1. Select \[**I**] to install all components that are not yet installed
    1. Select \[**6**] for number of quicksave slots

### Hidden Gameplay Options
Download [Hidden Gameplay Options](https://github.com/Argent77/A7-HiddenGameplayOptions/releases/latest)
1. Extract the zip file contents
1. Copy contents to your [Installation Directory](#installation-directory)
1. Run **setup-HiddenGameplayOptions.exe** from your [Installation Directory](#installation-directory).
    1. Select your language
        1. \[**0**] for English
    1. Select \[**N**] to bypass displaying of the readme.
    1. Select \[**I**] to install all Hidden Gameplay Options at once
    1. Select \[**N**] to skip key binding update (not sure what that does atm)

## Install a8_nio
1. Download [a8_nio.zip](https://gitlab.com/afrothundaaaa/a8_nio/-/releases/permalink/latest)
1. Extract the zip file contents
1. Copy the contents to your [Installation Directory](#installation-directory)
1. Create a copy of **weidu.exe**, and rename the copy to **setup-a8_nio.exe**
1. Run **setup-a8\_nio.exe** from your [Installation Directory](#installation-directory).
    1. Select \[**I**] for **Base Resource Pack**
    1. Select \[**I**] for **Baldur's Gate (2): Enhanced Edition**

> [!NOTE]
> For BGEE and BG2EE, you will need to install the base Resource Pack.
>
> The Title of the Second Installation will change, depending on the game.
>
> Example:
>> Baldur's Gate 2: Enhanced Edition for BG2EE