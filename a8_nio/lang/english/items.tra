////////////////////////////////////////////////////////////////////////////////////////////////////
//  Dawson
@100  = ~A gold band with a single blue topaz gem embedded. There appears to be weighing scales on either side of the gem. You simultaneously feel stronger and less hardy at the same time while placing it on your finger.~
@101  = ~Ring of the Righteous~
@102  = ~This ring once belonged to Nosra Brex of Luskan. The hale half-orc was renowned as the leader of a band of evenhanded peace keepers, who dared to defy the High Captains and piracy of her home. Her devotion to Tyr shined through in all of her actions, but even that devotion couldn't protect her from a dagger in the night. A brother paid in silver and with the promise of position ran her through as she slept. Treachery comes naturally to Luskan, and this ring slipped through his fingers and into the ocean as he was shot down by a rain of arrows. How it arrived here, is a mystery.

STATISTICS:

Equipped abilities:
– Strength: +1
– Maximum HP: -5
– Luck: +2

Weight: 0~

@103  = ~Amulet of the Devout~
@104  = ~This simple, round, silver amulet has a single inset, small, round sapphire. Its humble appearance seems to be complimented by a sense of safety when held.~
@105  = ~What could be said of Athalia Proudbottom was that she was a sensible, unpretentious halfling woman from Uthmere. Each day would begin and end with a prayer to Cyrrollalee, while the day was spent for making her home and tavern a place of respite for travelers weary from the road. Each night, when business was concluded she would open her hearth and leftover meals to the unfortunate without a roof of their own. Athalia passed this simple adornment onto her daughter as a ward from the evils of the road with a promise to pray each morning and night to their protector Cyrrollalee, and eventually to return home. How and when this amulet became enchanted is unknown, but it is said that Athalia spent many nights being regaled with tales from the road from a daughter eventually returned.

STATISTICS:

Equipped abilities:
– THAC0: +1
– Armor Class: +1
– Saving Throws: +1

Weight: 1~

@106  = ~Selfless +1~
@107  = ~This radiant shield seems to glow when placed in front of a comrade, but you cannot help but notice that your own armor slightly loses its shine when doing so. The radiance of this shield appears to emanate from the intricate, but simple, inlaid mithral designs, which belie its dwarven origins. Interestingly, a symbol of Torm made of mithral is found on the inside of the shield, passing through the enarmes.~
@108  = ~A surviving friend's tale of Dobrit Iceriver claims how a valiant act of selflessness led to a simple shield gaining the favor of a god not of Iceriver's own. Dobrit was a captain of the guard of the Mithral Hall when the dwarf mines broke into a shadowcavern and released the shadow dragon Shimmergloom. Shimmergloom and his duergar allies who viciously slaughtered all in sight, while Dobrit and his band fought valiantly to protect the Battlehammer dwarves. When his friends began falling one by one to the duergar horde, Iceriver began interposing himself and his shield between each blow intended for another. With each act, Iceriver paid the cost of blood that was owed by his brothers. Dobrit's friend, Gakkotir Woldbranch, swears that as the battle raged on, a previously unremarkable steel shield began to shine as did Dobrit's band of brothers. When Iceriver eventually fell, a final flash of light from the shield blinded the duergar in the vicinity, giving Gakkotir, the sole survivor of the band enough time to escape with the families under his protection. Woldbranch says that upon looking at the shield on the surface in the daylight, he saw new shining mithral lines inlaid in the shield, all of which converged on the back into the holy symbol of Torm. Gakkotir says the shield was lost to bandits while on the road to Silverymoon, but who believes an old drunkard in a tavern?

STATISTICS:

Equipped abilities:
– Armor Class +2

Charge abilities: 
– Rally the Meek 1/day 
  Chant
  Armor Class: -3 (Self) +3 (Party)
  Charm Immunity
Duration: 3 Turns

Weight: 6~

@109  = ~This ornate but poorly balanced weapon was found within the wreck of Balduran's ship. Gold weapons are not known to be useful in combat, but legends say that only weapons forged of gold can harm such dread creatures as the loup garou.

The weapon was reforged by Taerom Thunderhammer to be more useful in normal battles.  He seems to have lost some of its original power in doing so.

STATISTICS:

THAC0: +2
Damage: 2d4 +2
Damage type: Slashing
Speed Factor: 6
Proficiency Type: Bastard Sword
Type: One-handed
Requires:
 12 Strength

Weight: 12~
@110  = ~This ornate but poorly balanced weapon was found within the wreck of Balduran's ship. Gold weapons are not known to be useful in combat, but legends say that only weapons forged of gold can harm such dread creatures as the loup garou.

The weapon was reforged by Taerom Thunderhammer to be more useful in normal battles, and re-enchanted by Thalantyr of High Hedge.

STATISTICS:

Charge Abilities:
– Holy Word 3/day
  Doom & Ray of Enfeeblement

THAC0: +3
Damage: 2d4 +3 +2 Fire
Damage type: Slashing
Speed Factor: 8
Proficiency Type: Bastard Sword
Type: One-handed
Requires:
 12 Strength

Weight: 12~


@111  = ~This previously cursed weapon will, when a combat situation arises, imbue the wielder with an attack to an enemy and Backbiter will attack the wielder. For every attack made, Backbiter will attack the foe and also the wielder. This is a powerful spear, but one must ask if it is worth it in the end. The only way that Backbiter can be removed is by a Remove Curse spell.

STATISTICS:

Equipped abilities:

Combat abilities:
– 3 points of damage (piercing) inflicted to the target upon every successful hit
– 10% Chance to deal 3 points of damage (piercing) inflicted to the wielder

THAC0: +3
Damage: 1d6+3 (piercing) + 3 piercing
Speed Factor: 3
Proficiency Type: Spear
Type: Two-handed
Requires:
 5 Strength

Weight: 5~

////////////////////////////////////////////////////////////////////////////////////////////////////
//  Justin
@201  = ~Druid's Ring~
@202  = ~A druid order in Cormanthor is said to cautiously guard the secret to the construction of these rings, which have never been seen in the possession of anyone outside their immediate membership. How this particular ring escaped the protection of their forest is unknown, but there are rumors that an outcast could have brought it into exile with him. 

STATISTICS:

Equipped abilities:
– Can memorize one extra 4th and 5th-level priest spells

Weight: 0~

@203  = ~Belm +2~
@204  = ~Scimitar of Speed +2: Belm
This enchanted scimitar bears the mark of Taka Kobe, an honored swordsmith of Kara-Tur, though how it came to be here is unknown. Perfectly balanced, it affords quick recovery in combat and even allows the wielder time for an additional attack.

STATISTICS:

Combat abilities:
– 1 extra attack per round

THAC0: +2
Damage: 1d8+2 (slashing)
Speed Factor: 0
Proficiency Type: Scimitar/Wakizashi/Ninjatō
Type: One-handed
Requires:
 10 Strength

Weight: 3~

@205  = ~Gem of the Hunt~
@206  = ~Given to an order of druids that worship Malar, after they had successfully eradicated an order of Nobanion druids.  This 'gift' is a reminder not to trust demon lords, and it shows as you feel that when you are unlucky, you are VERY unlucky.

STATISTICS:

Combat Abilities
– 10% Chance of Feeblemind for 3 rounds on critical miss

Equipped abilities:
– Dexterity: +2
– Armor Class: +1
– Saving Throws: +1

Weight: 0~
@207  = ~Archdruid's Garb~
@208  = ~Gifted to an order of druids that lived in the Cloakwood forest, this cloak truly brings the wearer closer to nature, weaved from vines and enchanted with the spirit of the Feywild.  It seems to grant the user minor resistance to the elements.

STATISTICS:

Equipped abilities:
– Armor Class: +1
– Saving Throws: +1
– Resist Fire/Cold/Electric/Acid/Poison: +15%
– Wisdom: +1
– Luck: +2

Weight: 3~

@209  = ~Hide of the Wolfwere~
@210  = ~Skinned and cleaned, the Wolfwere's pelt lies before you, radiating a faint silver glow.  This hide seems to be thicker than most, giving extra protection against piercing attacks.  While wearing the armor, you seem to feel weaker, but are infused with a constant rush of energy.

STATISTICS:

Equipped abilities:
– Regenerate 1 Hit Points every 2 seconds
– Total Health: -10%
– Constitution: -2

Armor Class: 3
Requires:
 6 Strength

Weight: 35~

@211  = ~Elistraee's Pendant~
@212  = ~This shiny silvery blue pendant glows in the pale moonlight with a fierce radience.  It is said that this was a gift from Elistraee herself, when she walked the land in the Time of Troubles.

STATISTICS:

Equipped abilities: 
– Wisdom: +1
– THAC0: +1
– Armor Class: +1

Weight: 1~

@213  = ~Chanserv's Fish +2~
@214  = ~Those who claim something is better than a slap in the face with a wet fish never took a frozen fish to the noggin. This hefty halibut packs a wallop with a chill worthy of the North Wind.

STATISTICS:
– Wisdom: +1

THAC0: +3
Damage: 1d6+3, +1d4 cold damage
Damage type: Crushing
Speed Factor: 2
Proficiency Type: Club
Type: One-handed
Requires:
 5 Strength

Weight: 3~

@215  = ~Venom Laced Hatchet +3~
@216  = ~This hatchet was a gift from the dwarves to a group of noble gnomes that helped ward off an Elvish attack.  The gnomes used their impressive illusionary magics to confuse the elves, which would end up changing the tide of battle. The dwarves recruited the finest mages to help enchant this axe during its crafting. It is said to be made of an alloy consisting of silver, and another yet unknown material that is very hard to find, that only can be found deep in the Underdark.  The axe has ominous presence about it, and you aren't exactly sure what it means...
  
STATISTICS:

Combat Abilities:
– 33% Chance on hit to blur wielder
– Hit target suffers 1 point of poison damage per second for 15 seconds (Save vs. Poison negates)

THAC0: +3
Damage: 1d8+3 (slashing)
Speed Factor: 4
Proficiency Type: Axe
Type: One-handed
Requires:
 10 Strength

Weight: 4~

@217  = ~Cloakshadow's Shawl~
@218  = ~Forged by clerics of Baravar for a gnomish adventurer that proved his worth in battle.  A devastating fighter and an illusive mage, this cloak was designed to enhance the wearer's intelligence, but being donned by the great wizard, seems to have enhanced the ability for other gnomes.

STATISTICS:

Equipped abilities:
– Armor Class: +2
– Intelligence: +2 (gnomes) +1 (others)

Weight: 3~

@219  = ~Winter Wolf Hide~
@220  = ~While typically traded in the market, this Winter wolf hide was crafted into an extraordinary suit of hide armor.  You can almost feel the cold instantly repelled when you don this garb.

STATISTICS:

Equipped abilities:
– Cold Resist: +50%
– Open Locks: -10%
– Find Traps: -10%
– Pick Pockets: -10% 
– Move Silently: -20%

Armor Class: 4 (6 vs. piercing and missile)
Requires:
 6 Strength

Weight: 25~

////////////////////////////////////////////////////////////////////////////////////////////////////
//  Ian
@301  = ~Ilmare’s Folly~
@302  = ~Ilmare’s Folly:  Wild Mage necklace
This necklace was given to the little-known elven sorceress, Ilmare, by a githzerai named Unaloth.  In the year 1062 DR, She and her adventuring band were investigating the source of a violent slaad presence along the Tethyrian coast.  Upon quelling the invasion, they found the source was a rift leading to the outer plane of Limbo.  They were able to temporarily seal the rift, though it soon became clear that they lacked the knowledge or means for a permanent solution.  Seeking information, they planned to steal into the lair of a powerful necromancer who had supposedly been researching planar travel and obtain whatever knowledge they could.  No sooner than they entered, they found themselves within that very plane.  Escaping into the pure chaos of Limbo from the wrath of the wizard, they now found themselves with the knowledge they needed, but with no way home.  Fortunately, they would make their way to a githzerai fortress, where they would meet Unaloth.  Helping them route a band of githyanki from their realm earned Ilmare not only aid in returning to Faerun but this amulet as a small gift.  The amulet boosts a caster’s power and speed by a small amount, but it also seems to have absorbed a bit of the pure chaos of its home plane.  It is suspected that the disruption this caused in her spellcasting ability, ultimately led to the demise of Ilmare.  Whether by simply failing a spell when facing off against a powerful foe, or simply summoning a more dangerous monster by pure chance.  However, other rumors suggest she simply gave it away.

STATISTICS:

Equipped abilities:
– Wild Surge Bonus: -5%
– Caster Level: +1
– Improves casting speed by 1

Weight: 0~

@303  = ~Gloves of Pointing~
@304  = ~Gloves of Pointing: Ivankov's Hands
Emporio Ivankov has been thought as nothing more than an urban legend for most of Faerun’s peoples.  Supposedly, Ivankov hailed from a far-off kingdom in a tumultuous sea.  It is not known whether this enigmatic figure was a man or a woman, though based on the legend, he may have been able to change at will.  Ivankov is thought to have been a wizard of incredible or at least unique power.  Perhaps a Transmuter or an Enchanter capable of manipulating a person’s very biology.  Men could become women.  Women could become men.  The exhausted could be reinvigorated, reflexes could be heightened, a weakling could become incredibly strong, and even otherwise impossible to cure diseases could be wiped from their system…so long as their will to live was stronger than the pain they had to endure.  Reputedly a strong hand-to-hand combatant, Ivankov ruled unchallenged for many years until a disgruntled cook finally managed to overpower him…  These gloves are thought to have been the very same worn by Ivankov, taken by the usurper after his defeat.  Whether their power developed from simply being worn by the enigmatic figure or they were the source is not known.  Though their power has clearly waned in the following centuries.  A very strange tale indeed…

STATISTICS:

Charge abilities: 
– Random Debuff 1/day
  Ray of Enfeeblement
  Spook
  Blindness
  33% Chance of Wild Surge: Change Target's Gender

– Random Buff 1/day
  Luck
  Invisibility
  Protection From Evil
  33% Chance of Wild Surge: Change Wearer's Gender

– Slow Target for 3 rounds 1/day (Save vs. Wands neg.)
  33% Chance of Wild Surge: Change Target's Gender

Equipped abilities:
– Constitution: +1

Weight: 2~

@305  = ~Helm of the Holy +1~
@306  = ~Helm of the Holy:  Bloodhawk’s Crest
During the Time of Troubles, Tempus’s servant, the Red Lady, took possession of the Lady Kaitlin Tindall Bloodhawk, one of her most devout patrons.  While in this form she led Bloodhawk’s adventuring band to Tethyr, where they successfully repelled a massive army of monsters.  When she returned to Bloodhawk’s clan stronghold, she found it destroyed.  Upon reconstruction, the stronghold became a center of worship for the Red Lady known as the Citadel of Strategic Militancy.  This helmet was one of several created as a reward for followers of the Red lady after demonstrating great leadership in battle.

STATISTICS:

Equipped abilities:
– Turn Undead Level: +2
– Armor Class: +1
– Chant
– Protects against critical hits

Weight: 2~

@307  = ~Priest Ring~
@308  = ~Priest Ring:  Tempos’s Blessing
This ring is thought to have been gifted to the mightiest of Reghedmen shaman ages ago by their patron deity.  Known for their suspicion of the magical arts, they are not the most learned of folks, but their devotion to honor and the earth belies a deeper insight into the workings of the world.  Their intuitive understanding of the natural and divine magicks led their greatest shaman to cut out some of the flair in their casting in favor of efficiency.

STATISTICS:

Equipped abilities:
– Improves casting speed by 1
– Wisdom: +1
– Intelligence: -1

Weight: 0~

@309  = ~Darts of Annoyance +1~
@310  = ~Darts of Pestering: Annoyance +1
Little is known of where or when these darts were first developed, though they were clearly developed as some sort of practical joke.  They first became popular in the nation of Amn after which they gradually spread in underground markets along the Sword Coast.  One odd thing, noted by all who possess or have been victimized by these obnoxious darts, is that they smell faintly of turnips…

Combat abilities:
– Grants 5 attacks per round.


STATISTICS:

THAC0: +1
Damage: 0d0+0 (missile)
Speed Factor: 1
Proficiency Type: Dart
Type: One-handed

Weight: 0
~

////////////////////////////////////////////////////////////////////////////////////////////////////
//  Trevor
@401  = ~Myrkul's Guide~
@402  = ~Legends claim this black as night amulet grants the bearer unnatural precision in his strikes.  Further, some believe that Myrkul himself guides the wielder's hand.

STATISTICS:

Equipped abilities:
– Backstab Multiplier: +1
– THAC0: +1 (additional +1 with ranged weapons)
– Saving Throws: +1
– Maximum Health: -10


Weight: 1~

@403  = ~Cowl~
@404  = ~In da hood.

Fresh to Def.

STATISTICS:

Weight: 0~

@405  = ~Cow Kings Horns~
@406  = ~The origins of this helm are a mystery.  A few have claimed that it comes from another realm entirely, inhabited by a race of demonic cows called "Hell Bovine", and that they were cut from the Cow King himself once slain.

An inscription in the hood reads: Moo moo, moo moo moo?  MOO!

STATISTICS:

Equipped abilities:
– Armoo Class: +1
– THACm00: +1
– 10% Chance to Sumoon on critical moos

Charge abilities:
– Sumoon moore?
  Limooted Amoont

Weight: 1~

@407  = ~Deceiver's Ring~
@408  = ~A once notorious charlatan is believed to have worn this ring, and it appears to augment one's natural skills in subterfuge.

STATISTICS:

Equipped abilities:
– THAC0: +1
– Saving Throws: +1
– Thieving Skills: +20%

Charge Abilities:
- Once per day deal 5d6 unblockable damage to the target, and deal 5d4 damage to the wielder

Weight: 0~

@409  = ~Gloves of Chicanery~
@410  = ~These gloves with a slippery sheen seem to enhance the wearer's sleight of hand.
  
STATISTICS:

Equipped abilities:
– Dexterity: +1
– Pick Pockets: +50%

Weight: 2~

@411  = ~Ring of Invisibility: Sandthief's Ring 
Held by a master Thief for the better part of a generation, this ring was put to bold use in the markets of Waterdeep. Working a crowd in broad daylight, the rogue would steal countless numbers of purses from nobles, replacing them with bags of sand so the theft would go unnoticed. His identity was never known, but the name "Sandthief" was cursed loudly in its stead. It is rumored he retired, and now lives among the nobles he used to rob.

STATISTICS:

Charge abilities:
– Invisibility as per the namesake spell 5/day
– 33% Chance to blind enemy on backstab
– Pick Pockets: +25%

Weight: 0~

@412  = ~Cloak of Non-Detection: Whispers of Silence 
Reportedly created for a lineage of the greatest burglars ever to walk the night, this cloak was apparently a success. No record exists of previous owners.

STATISTICS:

Equipped abilities: 
– Non-detectable by magical means such as Detect Invisibility and scrying
– Saving Throws: +2 Dwarf/Halfling/Gnome (+1 others)
– Armor Class: +1

Weight: 3~