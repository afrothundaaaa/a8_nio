APPEND TAEROM
  IF ~~ THEN BEGIN a8SWDP11
      SAY @5111
        +~
          PartyGoldGT(2999)
          PartyHasItem("POTN13")
        ~+ @5112 GOTO a8SWDP12
        ++ @5113 DO ~AddJournalEntry(@5100, QUEST)~ GOTO 14
  END

  IF ~~ THEN BEGIN a8SWDP12
    SAY @5114 IF ~~ THEN DO ~
      TakePartyGold(3000)
      TakePartyItem("sw1h18")
      DestroyItem("sw1h18")
      TakePartyItem("potn13")
      DestroyItem("potn13")
      SetGlobal("a8SWDP1", "GLOBAL", 1)
      SetGlobalTimer("a8swdp1t1", "GLOBAL", ONE_DAY)
      AddJournalEntry(@5101, QUEST)
    ~ EXIT
  END

  IF ~~ THEN BEGIN a8SWDP13
    SAY @5116 IF ~~ THEN DO ~~ GOTO 14
  END

  IF WEIGHT #0 ~
      Global("a8SWDP1", "GLOBAL", 1)
      GlobalTimerExpired("a8swdp1t1", "GLOBAL")
    ~ THEN BEGIN a8SWDP14
    SAY @5117 IF ~~ THEN DO ~
      GiveItemCreate("a8swdp1a", LastTalkedToBy, 0, 0, 0)
      SetGlobal("a8SWDP1", "GLOBAL", 2)
      AddJournalEntry(@5102, QUEST)
    ~ EXIT
  END
END

EXTEND_BOTTOM TAEROM 14
  +~
    PartyHasItem("SW1H18")
    Global("a8SWDP1", "GLOBAL", 0)
  ~+ @5110 GOTO a8SWDP11
  +~
    Global("a8SWDP1","GLOBAL", 1)
    GlobalTimerNotExpired("a8swdp1t1", "GLOBAL")
  ~+ @5115 GOTO a8SWDP13
END